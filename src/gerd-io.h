/* Gerd - Gtk+ Event Recorder
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GERD_IO_H__
#define __GERD_IO_H__

#include        "gerd-events.h"


#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */



/* --- structures --- */
typedef enum
{
  GERD_CMD_EOF,
  GERD_CMD_DELAY,
  GERD_CMD_EVENT,
  GERD_CMD_WARP,
  GERD_CMD_PSTATE,
  GERD_CMD_LAST
} GerdCmdType;
struct _GerdCommand
{
  GerdCmdType	cmd;

  gchar	       *window_id;
  guint		retries;

  union {
    guint	delay;
    GdkEvent   *event;
    GerdPState  pstate;
    struct {
      gint      root_x;
      gint      root_y;
    } warp;
  } data;
};


/* --- record stream --- */
gpointer        gerd_stream_open        	(const gchar    *file_name);
void            gerd_stream_close       	(gpointer        stream);
void            gerd_stream_printf		(gpointer        stream,
						 const gchar    *format,
						 ...) G_GNUC_PRINTF (2,3);;
void		gerd_stream_put_delay		(gpointer	 stream,
						 guint		 msecs);
void		gerd_stream_put_event		(gpointer	 stream,
						 GdkEvent	*event);
void		gerd_stream_put_warp		(gpointer	 stream,
						 gint		 root_x,
						 gint		 root_y);
void		gerd_stream_put_pstate		(gpointer	 stream,
						 GdkWindow	*window,
						 gint		 win_x,
						 gint		 win_y,
						 guint		 mod_mask);


/* --- parser --- */
GScanner*       gerd_scanner_new		(const gchar	*file_name);
void		gerd_scanner_destroy		(GScanner	*scanner);
GerdCommand*	gerd_get_next_command		(GScanner	*scanner);
GerdCommand*	gerd_peek_command		(GScanner	*scanner);
void		gerd_free_command		(GerdCommand	*command);
gboolean	gerd_complete_command		(GScanner       *scanner,
						 GerdCommand	*command);

void		gerd_event_play			(GdkEvent	*event);
void		gerd_parser_add			(GScanner	*scanner);



#ifdef __cplusplus
#pragma {
}
#endif /* __cplusplus */

#endif /* __GERD_IO_H__ */
