/* Gerd - Gtk+ Event Recorder
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "gerd-events.h"

#include "gerd-io.h"
#include "gerd-xwrapper.h"
#include <gtk/gtkinvisible.h>
#include <stdlib.h>


#define	TIMER_OFF	~0
#define	RETRY_DELAY	750


/* --- prototypes --- */
static void		gerd_mask_pstate	(GdkWindow  **window,
						 gint        *win_x,
						 gint        *win_y,
						 gint        *root_x,
						 gint        *root_y,
						 guint       *mask);
static void		set_playback_timer	(guint	      msecs);
static void		main_event_handler	(GdkEvent    *event,
						 gpointer     data);
static guint		play_command		(GerdCommand *cmd);
static GdkFilterReturn	root_event_monitor	(GdkXEvent   *gdk_xevent,
						 GdkEvent    *event,
						 gpointer     gdk_root);


/* --- variables --- */
static gpointer    record_stream = NULL;
static gchar      *record_name = NULL;
static GScanner	  *playback_stream = NULL;
static gchar	  *playback_name = NULL;
static GTimer	  *event_timer = NULL;
static gulong      event_timer_stamp = 0;
static gulong      record_time_stamp = 0;
static GerdPState *masq_pstate = NULL;
static guint       marshal_timer_id = 0;


/* --- functions --- */
#include <gdk/gdkx.h>
void
gerd_events_init (void)
{
  static gboolean initialized = FALSE;

  if (!initialized)
    {
      initialized = TRUE;

      event_timer = g_timer_new ();
      gdk_event_handler_set (main_event_handler, NULL, NULL);

      gdk_root_add_watch (GDK_POINTER_MOTION_MASK, root_event_monitor, NULL);
    }
}

gulong
gerd_time_current (void)
{
  return event_timer_stamp + 1000 * g_timer_elapsed (event_timer, NULL);
}

static gulong
gerd_time_stamp_record (void)
{
  gulong cur_time;
  gulong delta;
  
  cur_time = gerd_time_current ();
  delta = cur_time - record_time_stamp;

  // DEBUG ("record %lu - %lu = %lu\n", cur_time, record_time_stamp, delta);

  record_time_stamp = cur_time;

  return delta;
}

static void
gerd_time_update (gulong current)
{
  gulong rec_lap = gerd_time_stamp_record ();

  // DEBUG ("stamp update %lu = %lu\n", event_timer_stamp, current);

  event_timer_stamp = current;
  record_time_stamp = event_timer_stamp - MIN (rec_lap, event_timer_stamp);
  
  g_timer_reset (event_timer);
}

void
gerd_masquerade_pstate (const GerdPState *pstate)
{
  if (pstate)
    {
      static GerdPState static_pstate;

      static_pstate = *pstate;
      masq_pstate = &static_pstate;
    }
  else
    masq_pstate = NULL;
}

void
gerd_set_recording (const gchar *file_name,
		    const gchar	*format,
		    ...)
{
  if (record_stream)
    {
      gerd_stream_close (record_stream);
      g_message ("Event recording to \"%s\" stopped", record_name);
      g_free (record_name);
      record_name = NULL;
      record_stream = NULL;
      GERD_SET_PSTATE_WRAPPER (NULL);
    }
  if (file_name)
    {
      gerd_set_playback (NULL);
      record_stream = gerd_stream_open (file_name);
      if (!record_stream)
	{
	  g_message ("Failed to open \"%s\" for event recording", file_name);
	  return;
	}

      record_name = g_strdup (file_name);
      g_message ("Event recording to \"%s\" started", record_name);

      if (format)
	{
	  gchar *greeting;
	  va_list args;

	  va_start (args, format);
	  greeting = g_strdup_vprintf (format, args);
	  va_end (args);

	  gerd_stream_printf (record_stream, "%s", greeting);

	  g_free (greeting);
	}
      
      gerd_stream_put_delay (record_stream, 1500);
      GERD_SET_PSTATE_WRAPPER (gerd_mask_pstate);
    }
}

void
gerd_set_playback (const gchar *file_name)
{
  if (playback_stream)
    {
      gerd_scanner_destroy (playback_stream);
      g_message ("Event playback from \"%s\" stopped", playback_name);
      g_free (playback_name);
      playback_stream = NULL;
      playback_name = NULL;
      masq_pstate = NULL;
      GERD_SET_PSTATE_WRAPPER (NULL);
      set_playback_timer (TIMER_OFF);

      if (gerd_debug_flags & GERD_FEAT_FORCE_EXIT)
	exit (0);
    }
  if (file_name)
    {
      gerd_set_recording (NULL, NULL);
      playback_stream = gerd_scanner_new (file_name);
      if (!playback_stream)
	{
	  g_message ("Failed to open \"%s\" for event playback", file_name);
	  return;
	}

      playback_name = g_strdup (file_name);
      playback_stream->input_name = playback_name;
      g_message ("Event playback from \"%s\" started", playback_name);

      GERD_SET_PSTATE_WRAPPER (gerd_mask_pstate);
      set_playback_timer (0);
    }
}

static void
main_event_handler (GdkEvent *event,
		    gpointer  data)
{
  GdkWindow *window;
  GtkWidget *widget;
  guint32 event_time;

  g_return_if_fail (event != NULL);

  window = event->any.window;
  widget = gtk_get_event_widget (event);

  event_time = gdk_event_get_time (event);
  if (event_time)
    gerd_time_update (event_time);

  if (widget)
    switch (event->type)
      {
      case GDK_MAP:
	if (!widget->parent)
	  {
	    gerd_toplevel_ensure_unique_id (widget);
	    
	    if (gerd_widget_get_window_pos (widget, window) < 0)
	      gerd_widget_add_window (widget, window);

	    if (record_stream && GTK_IS_WINDOW (widget) &&
		GTK_WINDOW (widget)->type == GTK_WINDOW_TOPLEVEL)
	      {
		GdkEvent qevent = { 0, };
		gint x, y, width, height;
		
		/* now we're mapped, need to queue a fake configure event
		 * so we popup at correct position/size upon playback.
		 */
		qevent.type = GDK_CONFIGURE;
		qevent.configure.window = widget->window;
		qevent.configure.send_event = FALSE;
		gdk_window_get_position (widget->window,
					 &x,
					 &y);
		gdk_window_get_size (widget->window,
				     &width,
				     &height);
		qevent.configure.x = x;
		qevent.configure.y = y;
		qevent.configure.width = width;
		qevent.configure.height = height;
		
		gerd_stream_put_delay (record_stream, gerd_time_stamp_record () + 150);
		gerd_stream_put_event (record_stream, &qevent);
	      }
	  }
	else if (gerd_widget_get_window_pos (widget, window) < 0)
	  gerd_widget_add_window (widget, window);
	break;
      case GDK_CONFIGURE:
	/* we only need to trap user caused configure events
	 * on GtkWindow *after* we received a GDK_MAP for it
	 * need to perform gtk_window_configure_event() alike
	 * checks for configure_request_count here
	 */
	if (record_stream && widget->parent == NULL &&
	    widget->window == event->configure.window &&
	    gerd_widget_get_window_pos (widget, window) == 0 &&
	    GTK_IS_WINDOW (widget) && GTK_WINDOW (widget)->type == GTK_WINDOW_TOPLEVEL &&
	    GTK_WINDOW (widget)->configure_request_count == 0)
	  {
	    gerd_stream_put_delay (record_stream, gerd_time_stamp_record ());
	    gerd_stream_put_event (record_stream, event);
	  }
	break;
      case GDK_MOTION_NOTIFY:
      case GDK_BUTTON_RELEASE:
      case GDK_KEY_RELEASE:
	if (record_stream)
	  {
	    gerd_stream_put_delay (record_stream, gerd_time_stamp_record ());
	    gerd_stream_put_warp (record_stream, event->motion.x_root, event->motion.y_root);
	    gerd_stream_put_event (record_stream, event);
	  }
	if (!event->any.send_event && playback_stream)
	  event = NULL;
	break;
      case GDK_BUTTON_PRESS:
      case GDK_2BUTTON_PRESS:
      case GDK_3BUTTON_PRESS:
      case GDK_KEY_PRESS:
	if (record_stream)
	  {
	    gerd_stream_put_delay (record_stream, gerd_time_stamp_record ());
	    gerd_stream_put_event (record_stream, event);
	  }
	if (!event->any.send_event && playback_stream)
	  gerd_set_playback (NULL);
	break;
      case GDK_ENTER_NOTIFY:
      case GDK_LEAVE_NOTIFY:
	if (record_stream)
	  {
	    gerd_stream_put_delay (record_stream, gerd_time_stamp_record ());
	    gerd_stream_put_warp (record_stream, event->crossing.x_root, event->crossing.y_root);
            gerd_stream_put_event (record_stream, event);
	  }
	if (playback_stream)
	  event = NULL;
	break;
      case GDK_DELETE:
	if (record_stream)
	  {
	    gerd_stream_put_delay (record_stream, gerd_time_stamp_record ());
	    gerd_stream_put_event (record_stream, event);
	  }
	if (playback_stream)
	  event = NULL;
	break;
      case GDK_DESTROY:
	gerd_widget_remove_window (widget, window); // FIXME: isn't triggered
	break;
      default:
	break;
      }
  
  if (event)
    {
      if (masq_pstate)
	gdk_event_set_state (event, masq_pstate->mod_mask);
      
      gtk_main_do_event (event);
    }
}

static void
play_event (GdkEvent *event)
{
  GtkWidget *widget;

  g_return_if_fail (event != NULL);

  widget = gtk_get_event_widget (event);
  switch (event->type)
    {
    case GDK_CONFIGURE:
      DEBUG ("PLAY: Configure Event\n");
      gdk_window_move_resize (event->configure.window,
			      event->configure.x, event->configure.y,
			      event->configure.width, event->configure.height);
      break;
    case GDK_MOTION_NOTIFY:
      DEBUG ("PLAY: Motion Event\n");
      // gdk_root_warp_pointer (event->motion.x_root, event->motion.y_root);
      gdk_flush ();
      if (GTK_IS_WINDOW (widget))
	gdk_window_raise (widget->window);
      while (gdk_events_pending ())
	g_main_iteration (FALSE);
      event->motion.time = gerd_time_current ();
      gtk_main_do_event (event);
      break;
    case GDK_BUTTON_PRESS:
    case GDK_2BUTTON_PRESS:
    case GDK_3BUTTON_PRESS:
    case GDK_BUTTON_RELEASE:
      DEBUG ("PLAY: Button Event\n");
      if (GTK_IS_WINDOW (widget))
	gdk_window_raise (widget->window);
      while (gdk_events_pending ())
	g_main_iteration (FALSE);
      event->button.time = gerd_time_current ();
      gtk_main_do_event (event);
      break;
    case GDK_ENTER_NOTIFY:
    case GDK_LEAVE_NOTIFY:
      DEBUG ("PLAY: Crossing Event\n");
      while (gdk_events_pending ())
	g_main_iteration (FALSE);
      event->crossing.time = gerd_time_current ();
      gtk_main_do_event (event);
      break;
    case GDK_KEY_PRESS:
    case GDK_KEY_RELEASE:
      DEBUG ("PLAY: Key Event\n");
      if (GTK_IS_WINDOW (widget))
	gdk_window_raise (widget->window);
      gdk_flush ();
      while (gdk_events_pending ())
	g_main_iteration (FALSE);
      event->key.time = gerd_time_current ();
      gtk_main_do_event (event);
      break;
    case GDK_DELETE:
      DEBUG ("PLAY: Delete Window\n");
      gtk_main_do_event (event);
      break;
    default:
      g_warning (G_STRLOC ": cannot play event type %d", event->type);
      break;
    }
}

static guint
play_command (GerdCommand *cmd)
{
  guint msecs = 0;

  g_return_val_if_fail (cmd != NULL, TIMER_OFF);

  switch (cmd->cmd)
    {
    case GERD_CMD_DELAY:
      msecs = cmd->data.delay;
      DEBUG ("PLAY: delay %u\n", msecs);
      break;
    case GERD_CMD_EVENT:
      play_event (cmd->data.event);
      break;
    case GERD_CMD_WARP:
      DEBUG ("PLAY: Warp\n");
      gdk_root_warp_pointer (cmd->data.warp.root_x, cmd->data.warp.root_y);
      break;
    case GERD_CMD_PSTATE:
      DEBUG ("PLAY: Pointer State\n");
      gerd_masquerade_pstate (&cmd->data.pstate);
      break;
    case GERD_CMD_EOF:
      DEBUG ("PLAY: end of file\n");
      gerd_set_playback (NULL);
      msecs = TIMER_OFF;
      break;
    case GERD_CMD_LAST:
      /* ... */
      break;
    }

  return msecs;
}

static gboolean
do_command (gpointer data)
{
  GerdCommand *cmd;

  set_playback_timer (TIMER_OFF);

  cmd = gerd_peek_command (playback_stream);
  if (cmd && gerd_complete_command (playback_stream, cmd))
    {
      guint msecs;

      cmd = gerd_get_next_command (playback_stream);
      msecs = play_command (cmd);
      gerd_free_command (cmd);
      set_playback_timer (msecs);
    }
  else if (cmd)
    {
      static guint failures = 0;

      DEBUG ("RETRY, failed to lookup window: %s\n", cmd->window_id);
      if (cmd->retries >= 5)
	{
	  DEBUG ("SKIPPING COMMAND, too many retries\n");
	  cmd = gerd_get_next_command (playback_stream);
	  gerd_free_command (cmd);
	  failures++;
	  if (failures >= 5)
	    {
	      DEBUG ("ABORTING, too many failures\n");
	      gerd_set_playback (NULL);
	    }
	}
      set_playback_timer (RETRY_DELAY);
    }
  else
    gerd_set_playback (NULL);

  return FALSE;
}

static GdkFilterReturn
root_event_monitor (GdkXEvent *gdk_xevent,
		    GdkEvent  *event,
		    gpointer   gdk_root)
{
  XEvent *xevent = gdk_xevent;

  if (record_stream && xevent->type == MotionNotify)
    {
      gerd_stream_put_delay (record_stream, gerd_time_stamp_record ());
      gerd_stream_put_warp (record_stream, xevent->xmotion.x_root, xevent->xmotion.y_root);
    }

  return GDK_FILTER_CONTINUE;
}

static void
gerd_mask_pstate (GdkWindow **window,
		  gint       *win_x,
		  gint       *win_y,
		  gint       *root_x,
		  gint       *root_y,
		  guint      *mask)
{
  if (record_stream)
    gerd_stream_put_pstate (record_stream, *window, *win_x, *win_y, *mask);

  if (playback_stream && masq_pstate)
    {
      GerdCommand *cmd = gerd_peek_command (playback_stream);

      if (cmd && cmd->cmd == GERD_CMD_PSTATE)
	do_command (NULL);
  
      *window = masq_pstate->window;
      *win_x = masq_pstate->win_x;
      *win_y = masq_pstate->win_y;
      *mask = masq_pstate->mod_mask;

      *root_x = *win_x;
      *root_y = *win_y;
      gdk_window_translate (*window, NULL, root_x, root_y);
    }
}

static void
set_playback_timer (guint msecs)
{
  if (marshal_timer_id)
    {
      g_source_remove (marshal_timer_id);
      marshal_timer_id = 0;
    }

  if (playback_stream && msecs != TIMER_OFF)
    marshal_timer_id = g_timeout_add_full (G_PRIORITY_DEFAULT,
					   msecs,
					   do_command,
					   NULL, NULL);
}
