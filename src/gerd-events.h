/* Gerd - Gtk+ Event Recorder
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GERD_EVENTS_H__
#define __GERD_EVENTS_H__


#include        "gerd-main.h"


#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */


/* --- structures --- */
struct _GerdPState
{
  GdkWindow *window;
  gint	     win_x;
  gint	     win_y;
  guint	     mod_mask;
};


/* --- prototypes --- */
void	gerd_events_init	 (void);
gulong	gerd_time_current	 (void);
void	gerd_set_playback	 (const gchar	  *file_name);
void	gerd_set_recording	 (const gchar	  *file_name,
				  const gchar	  *format,
				  ...);
void	gerd_masquerade_pstate	 (const GerdPState *pstate);




#ifdef __cplusplus
#pragma {
}
#endif /* __cplusplus */

#endif /* __GERD_EVENTS_H__ */
