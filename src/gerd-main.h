/* Gerd - Gtk+ Event Recorder
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GERD_MAIN_H__
#define __GERD_MAIN_H__

#include        "gerd-utils.h"


#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */



/* --- typedefs --- */
typedef struct _GerdPState  GerdPState;
typedef struct _GerdCommand GerdCommand;
typedef enum
{
  GERD_FEAT_VERBOSE		= 1 << 0,
  GERD_FEAT_FORCE_EXIT		= 1 << 1
} GerdDebugFlags;
extern GerdDebugFlags gerd_debug_flags;


/* --- prototypes --- */



#ifdef __cplusplus
#pragma {
}
#endif /* __cplusplus */

#endif /* __GERD_MAIN_H__ */
