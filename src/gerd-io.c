/* Gerd - Gtk+ Event Recorder
 * Copyright (C) 2000 Red Hat, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "gerd-io.h"

#include "gerd-events.h"
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>


/* --- defines --- */
#define	parse_error(scanner, token) g_scanner_unexp_token (scanner, token, "command", "keyword", NULL, NULL, TRUE)


/* --- prototypes --- */
static gchar*		input_source_name	(GdkInputSource	 source);
static gchar*		strdup_quoted		(const gchar	*string);
static void		free_event		(GdkEvent	*event);
static gboolean		dump_event		(GdkEvent	*event,
						 GString	*gstring);
static GdkEvent*	parse_event		(GScanner	*scanner,
						 gchar	       **window_p);
static inline gint	scan_int		(GScanner	*scanner);
static inline gfloat	scan_float		(GScanner	*scanner);
static inline gchar*	scan_string		(GScanner	*scanner);
static inline gint	scan_symbol_int		(GScanner	*scanner);


/* --- variables --- */
static GScannerConfig scanner_config =
{
  (
   " \t\r\n"
   )                    /* cset_skip_characters */,
  (
   G_CSET_a_2_z
   "_"
   G_CSET_A_2_Z
   )                    /* cset_identifier_first */,
  (
   G_CSET_a_2_z
   "-_0123456789"
   G_CSET_A_2_Z
   G_CSET_LATINS
   G_CSET_LATINC
   )                    /* cset_identifier_nth */,
  ( "#\n" )             /* cpair_comment_single */,

  FALSE                 /* case_sensitive */,

  TRUE                  /* skip_comment_multi */,
  TRUE                  /* skip_comment_single */,
  TRUE                  /* scan_comment_multi */,
  TRUE                  /* scan_identifier */,
  FALSE                 /* scan_identifier_1char */,
  FALSE                 /* scan_identifier_NULL */,
  TRUE                  /* scan_symbols */,
  FALSE                 /* scan_binary */,
  TRUE                  /* scan_octal */,
  TRUE                  /* scan_float */,
  TRUE                  /* scan_hex */,
  FALSE                 /* scan_hex_dollar */,
  TRUE                  /* scan_string_sq */,
  TRUE                  /* scan_string_dq */,
  TRUE                  /* numbers_2_int */,
  FALSE                 /* int_2_float */,
  FALSE                 /* identifier_2_string */,
  TRUE                  /* char_2_token */,
  FALSE                 /* symbol_2_token */,
  FALSE                 /* scope_0_fallback */,
};
static const struct {
  gint   n;
  gchar *name;
} input_sources[] = {
  { GDK_SOURCE_MOUSE,			"Mouse", },
  { GDK_SOURCE_PEN,			"Pen", },
  { GDK_SOURCE_ERASER,			"Eraser", },
  { GDK_SOURCE_CURSOR,			"Cursor", },
}, crossing_mode[] = {
  { GDK_CROSSING_NORMAL,		"Normal", },
  { GDK_CROSSING_GRAB,			"Grab", },
  { GDK_CROSSING_UNGRAB,		"Ungrab", },
}, notify_detail[] = {
  { GDK_NOTIFY_ANCESTOR,		"Anchestor", },
  { GDK_NOTIFY_VIRTUAL,			"Virtual", },
  { GDK_NOTIFY_INFERIOR,		"Inferior", },
  { GDK_NOTIFY_NONLINEAR,		"Nonlinear", },
  { GDK_NOTIFY_NONLINEAR_VIRTUAL,	"Virtual", },
  { GDK_NOTIFY_UNKNOWN,			"Unknown", },
};


/* --- functions --- */
static gchar*
input_source_name (GdkInputSource source)
{
  if (source < 0 || source >= sizeof (input_sources) / sizeof (input_sources[0]))
    return NULL;
  else
    return input_sources[source].name;
}

static gchar*
crossing_mode_name (GdkCrossingMode mode)
{
  if (mode < 0 || mode >= sizeof (crossing_mode) / sizeof (crossing_mode[0]))
    return NULL;
  else
    return crossing_mode[mode].name;
}

static gchar*
notify_detail_name (GdkNotifyType detail)
{
  if (detail < 0 || detail >= sizeof (notify_detail) / sizeof (notify_detail[0]))
    return NULL;
  else
    return notify_detail[detail].name;
}

static gchar*
strdup_quoted (const gchar *string)
{
  GString *gstring;
  gchar *retval;

  if (!string)
    return NULL;

  gstring = g_string_new ("");
  while (*string)
    {
      switch (*string)
	{
	case '\\':
	  g_string_append (gstring, "\\\\");
	  break;
	case '\t':
	  g_string_append (gstring, "\\t");
	  break;
	case '\n':
	  g_string_append (gstring, "\\n");
	  break;
	case '\r':
	  g_string_append (gstring, "\\r");
	  break;
	case '\b':
	  g_string_append (gstring, "\\b");
	  break;
	case '\f':
	  g_string_append (gstring, "\\f");
	  break;
	default:
	  if (*string > 126 || *string < 32)
	    g_string_sprintfa (gstring, "\\%03o", (guchar) *string);
	  else
	    g_string_append_c (gstring, *string);
	  break;
	}
      string++;
    }

  retval = gstring->str;
  g_string_free (gstring, FALSE);

  return retval;
}

gpointer
gerd_stream_open (const gchar *file_name)
{
  FILE *file;

  g_return_val_if_fail (file_name, NULL);

  file = fopen (file_name, "w");

  return file;
}

void
gerd_stream_printf (gpointer     stream,
		    const gchar *format,
		    ...)
{
  FILE *file = stream;
  gchar *string;
  va_list args;

  g_return_if_fail (stream != NULL);
  g_return_if_fail (format != NULL);
  
  va_start (args, format);
  string = g_strdup_vprintf (format, args);
  va_end (args);
  
  fputs (string, file);
  g_free (string);
}

void
gerd_stream_close (gpointer stream)
{
  FILE *file = stream;

  g_return_if_fail (stream != NULL);

  fclose (file);
}

GScanner*
gerd_scanner_new (const gchar *file_name)
{
  GScanner *scanner;
  gint i, fd;

  fd = open (file_name, O_RDONLY);
  if (fd < 0)
    return NULL;

  scanner = g_scanner_new (&scanner_config);
  g_scanner_input_file (scanner, fd);
  scanner->user_data = NULL;	/* peeked commands */
  
  for (i = 0; i < sizeof (input_sources) / sizeof (input_sources[0]); i++)
    g_scanner_scope_add_symbol (scanner, 0, input_sources[i].name, GINT_TO_POINTER (input_sources[i].n));
  for (i = 0; i < sizeof (crossing_mode) / sizeof (crossing_mode[0]); i++)
    g_scanner_scope_add_symbol (scanner, 0, crossing_mode[i].name, GINT_TO_POINTER (crossing_mode[i].n));
  for (i = 0; i < sizeof (notify_detail) / sizeof (notify_detail[0]); i++)
    g_scanner_scope_add_symbol (scanner, 0, notify_detail[i].name, GINT_TO_POINTER (notify_detail[i].n));
  g_scanner_scope_add_symbol (scanner, 0, "BUTTON_PRESS", GINT_TO_POINTER (GDK_BUTTON_PRESS));
  g_scanner_scope_add_symbol (scanner, 0, "DOUBLE_BUTTON_PRESS", GINT_TO_POINTER (GDK_2BUTTON_PRESS));
  g_scanner_scope_add_symbol (scanner, 0, "TRIPLE_BUTTON_PRESS", GINT_TO_POINTER (GDK_3BUTTON_PRESS));
  g_scanner_scope_add_symbol (scanner, 0, "BUTTON_RELEASE", GINT_TO_POINTER (GDK_BUTTON_RELEASE));
  g_scanner_scope_add_symbol (scanner, 0, "KEY_PRESS", GINT_TO_POINTER (GDK_KEY_PRESS));
  g_scanner_scope_add_symbol (scanner, 0, "KEY_RELEASE", GINT_TO_POINTER (GDK_KEY_RELEASE));
  g_scanner_scope_add_symbol (scanner, 0, "ENTER", GINT_TO_POINTER (GDK_ENTER_NOTIFY));
  g_scanner_scope_add_symbol (scanner, 0, "LEAVE", GINT_TO_POINTER (GDK_LEAVE_NOTIFY));
  g_scanner_scope_add_symbol (scanner, 0, "MOTION_NOTIFY", GINT_TO_POINTER (GDK_MOTION_NOTIFY));
  g_scanner_scope_add_symbol (scanner, 0, "CONFIGURE", GINT_TO_POINTER (GDK_CONFIGURE));
  g_scanner_scope_add_symbol (scanner, 0, "DELETE", GINT_TO_POINTER (GDK_DELETE));
  g_scanner_scope_add_symbol (scanner, 0, "TRUE", GINT_TO_POINTER (TRUE));
  g_scanner_scope_add_symbol (scanner, 0, "FALSE", GINT_TO_POINTER (FALSE));

  return scanner;
}

void
gerd_scanner_destroy (GScanner *scanner)
{
  g_return_if_fail (scanner != NULL);

  if (scanner->user_data)
    gerd_free_command (scanner->user_data);

  close (scanner->input_fd);

  g_scanner_destroy (scanner);
}

static inline gint
scan_int (GScanner *scanner)
{
  if (!scanner->parse_errors)
    {
      gboolean negate = FALSE;
      
      if (g_scanner_peek_next_token (scanner) == '-')
	{
	  g_scanner_get_next_token (scanner);
	  negate = TRUE;
	}
      
      if (g_scanner_get_next_token (scanner) != G_TOKEN_INT)
	parse_error (scanner, G_TOKEN_INT);
      
      if (!scanner->parse_errors)
	return negate ? - scanner->value.v_int : scanner->value.v_int;
    }
  
  return 0;
}

static inline gfloat
scan_float (GScanner *scanner)
{
  if (!scanner->parse_errors)
    {
      gboolean negate = FALSE;
      
      if (g_scanner_peek_next_token (scanner) == '-')
	{
	  g_scanner_get_next_token (scanner);
	  negate = TRUE;
	}
      
      g_scanner_get_next_token (scanner);
      if (scanner->token != G_TOKEN_FLOAT && scanner->token != G_TOKEN_INT)
	parse_error (scanner, G_TOKEN_FLOAT);
      
      if (!scanner->parse_errors)
	{
	  gfloat f = scanner->token == G_TOKEN_FLOAT ? scanner->value.v_float : scanner->value.v_int;
	  
	  return negate ? - f : f;
	}
    }
  
  return 0;
}

static inline gchar*
scan_string (GScanner *scanner)
{
  if (!scanner->parse_errors)
    {
      if (g_scanner_get_next_token (scanner) != G_TOKEN_STRING)
	parse_error (scanner, G_TOKEN_STRING);
      
      if (!scanner->parse_errors)
	return scanner->value.v_string;
    }
  
  return NULL;
}

static inline gint
scan_symbol_int (GScanner *scanner)
{
  if (!scanner->parse_errors)
    {
      if (g_scanner_get_next_token (scanner) != G_TOKEN_SYMBOL)
	parse_error (scanner, G_TOKEN_SYMBOL);
      
      if (!scanner->parse_errors)
	return GPOINTER_TO_INT (scanner->value.v_symbol);
    }
  
  return 0;
}

static void
free_event (GdkEvent *event)
{
  g_return_if_fail (event != NULL);
  
  if (event->type == GDK_KEY_PRESS || event->type == GDK_KEY_RELEASE)
    g_free (event->key.string);
  g_free (event);
}

void
gerd_stream_put_delay (gpointer stream,
		       guint    msecs)
{
  g_return_if_fail (stream != NULL);

  gerd_stream_printf (stream, "(wait %u)\n", msecs);
}

void
gerd_stream_put_event (gpointer  stream,
		       GdkEvent *event)
{
  GString *gstring;

  g_return_if_fail (stream != NULL);
  g_return_if_fail (event != NULL);

  gstring = g_string_sized_new (256);

  if (dump_event (event, gstring))
    gerd_stream_printf (stream, "(event %s)\n", gstring->str);

  g_string_free (gstring, TRUE);
}

void
gerd_stream_put_warp (gpointer stream,
		      gint     root_x,
		      gint     root_y)
{
  g_return_if_fail (stream != NULL);

  gerd_stream_printf (stream, "(warp %d %d)\n", root_x, root_y);
}

void
gerd_stream_put_pstate (gpointer   stream,
			GdkWindow *window,
			gint       win_x,
			gint       win_y,
			guint      mod_mask)
{
  gchar *window_name, *t;

  g_return_if_fail (stream != NULL);

  t = gerd_window_create_path (window);
  window_name = strdup_quoted (t);
  g_free (t);

  gerd_stream_printf (stream, "(pstate \"%s\" %d %d %u)\n", window_name, win_x, win_y, mod_mask);
  g_free (window_name);
}

static gboolean
dump_event (GdkEvent *event,
	    GString  *gstring)
{
  gchar *window_name, *t;
  
  g_return_val_if_fail (event != NULL, FALSE);
  g_return_val_if_fail (event->any.window != NULL, FALSE);
  g_return_val_if_fail (gstring != NULL, FALSE);
  
  t = gerd_window_create_path (event->any.window);
  window_name = strdup_quoted (t);
  
  /* do some paranoia checks */
  if (gerd_window_from_path (t) != event->any.window)
    {
      gdk_beep ();
      DEBUG ("*** WARNING ***: Skipping event, RLOOKUP FAILED: %s", window_name);

      g_free (t);
      g_free (window_name);

      return FALSE;
    }
  
  g_free (t);
  
  g_string_append_c (gstring, '(');
  switch (event->type)
    {
      gchar *string, *subwindow_name;
    case GDK_2BUTTON_PRESS:
    case GDK_3BUTTON_PRESS:
      g_string_append (gstring, event->type == GDK_2BUTTON_PRESS ? "DOUBLE_" : "TRIPLE_");
    case GDK_BUTTON_PRESS:
    case GDK_BUTTON_RELEASE:
      {
      gdouble pressure = 0.0,
	      xtilt = 0.0,
	      ytilt = 0.0;
      gdk_device_get_axis(event->button.device, event->button.axes, GDK_AXIS_PRESSURE, &pressure);
      gdk_device_get_axis(event->button.device, event->button.axes, GDK_AXIS_XTILT, &xtilt);
      gdk_device_get_axis(event->button.device, event->button.axes, GDK_AXIS_YTILT, &ytilt);
      g_string_sprintfa (gstring,
			 "BUTTON_%s \"%s\" "
			 "%s "
			 "%g %g "
			 "%g %g %g "
			 "%u %u "
			 "%s \"%s\"",
			 event->type == GDK_BUTTON_RELEASE ? "RELEASE" : "PRESS", window_name,
			 event->button.send_event ? "TRUE" : "FALSE",
			 event->button.x, event->button.y,
			 pressure, xtilt, ytilt,
			 event->button.state, event->button.button,
      			 input_source_name (event->button.device->source), event->button.device->name);
      }
      break;
    case GDK_CONFIGURE:
      /*if(GTK_WIDGET_MAPPED())*/
        g_string_sprintfa (gstring,
			 "CONFIGURE \"%s\" "
			 "%s "
			 "%u %u "
			 "%u %u",
			 window_name,
			 event->motion.send_event ? "TRUE" : "FALSE",
			 event->configure.x, event->configure.y,
			 event->configure.width, event->configure.height);
      break;
    case GDK_MOTION_NOTIFY:
      {
      gdouble pressure = 0.0,
	      xtilt = 0.0,
	      ytilt = 0.0;
      gdk_device_get_axis(event->button.device, event->button.axes, GDK_AXIS_PRESSURE, &pressure);
      gdk_device_get_axis(event->button.device, event->button.axes, GDK_AXIS_XTILT, &xtilt);
      gdk_device_get_axis(event->button.device, event->button.axes, GDK_AXIS_YTILT, &ytilt);
      g_string_sprintfa (gstring,
			 "MOTION_NOTIFY \"%s\" "
			 "%s "
			 "%g %g "
			 "%g %g %g "
			 "%u %u "
			 "%s \"%s\"",
			 window_name,
			 event->motion.send_event ? "TRUE" : "FALSE",
			 event->motion.x, event->motion.y,
			 pressure, xtilt, ytilt,
			 event->motion.state, event->motion.is_hint,
      			 input_source_name (event->motion.device->source), event->motion.device->name);
      }
      break;
    case GDK_KEY_PRESS:
    case GDK_KEY_RELEASE:
      string = strdup_quoted (event->key.string);
      g_string_sprintfa (gstring,
			 "KEY_%s \"%s\" "
			 "%s "
			 "%u %u %d "
			 "\"%s\"",
			 event->type == GDK_KEY_PRESS ? "PRESS" : "RELEASE", window_name,
			 event->motion.send_event ? "TRUE" : "FALSE",
			 event->key.state, event->key.keyval, event->key.length,
			 string);
      g_free (string);
      break;
    case GDK_ENTER_NOTIFY:
    case GDK_LEAVE_NOTIFY:
      string = gerd_window_create_path (event->crossing.subwindow);
      subwindow_name = strdup_quoted (string);
      g_free (string);
      g_string_sprintfa (gstring,
			 "%s \"%s\" "
			 "%s \"%s\" "
			 "%g %g "
			 "%s %s "
			 "%s %u",
			 event->type == GDK_ENTER_NOTIFY ? "ENTER" : "LEAVE", window_name,
			 event->crossing.send_event ? "TRUE" : "FALSE", subwindow_name,
			 event->crossing.x, event->crossing.y,
			 crossing_mode_name (event->crossing.mode), notify_detail_name (event->crossing.detail),
			 event->crossing.focus ? "TRUE" : "FALSE", event->crossing.state);
      g_free (subwindow_name);
      break;
    case GDK_DELETE:
      g_string_sprintfa (gstring, "DELETE \"%s\" %s", window_name, event->any.send_event ? "TRUE" : "FALSE");
      break;
    default:
      g_warning (G_STRLOC ": cannot handle event type %d", event->type);
      break;
    }
  g_string_append_c (gstring, ')');
  
  g_free (window_name);

  return TRUE;
}

static GdkEvent*
parse_event (GScanner *scanner,
	     gchar   **window_p)
{
  GdkEvent *event;

  g_return_val_if_fail (scanner != NULL, NULL);
  g_return_val_if_fail (window_p != NULL, NULL);
  
  if (g_scanner_get_next_token (scanner) != '(')
    {
      parse_error (scanner, '(');
      
      return NULL;
    }
  
  event = g_new0 (GdkEvent, 1);
  
  event->any.type = scan_symbol_int (scanner);
  event->any.window = NULL;
  *window_p = g_strdup (scan_string (scanner));
  event->any.send_event = scan_symbol_int (scanner) != FALSE;
  if (!scanner->parse_errors)
    switch (event->type)
      {
	gchar *subwindow_name;
      case GDK_BUTTON_PRESS:
      case GDK_2BUTTON_PRESS:
      case GDK_3BUTTON_PRESS:
      case GDK_BUTTON_RELEASE:
	event->button.x = scan_float (scanner);
	event->button.y = scan_float (scanner);
	/*event->button.pressure =*/ scan_float (scanner);
	/*event->button.xtilt =*/ scan_float (scanner);
	/*event->button.ytilt =*/ scan_float (scanner);
	event->button.state = scan_int (scanner);
	event->button.button = scan_int (scanner);
#warning "HERZI: add support for the device (maybe by name)"
	/*event->button.source =*/ scan_symbol_int (scanner);
#warning "HERZI: free this?"
	/*event->button.deviceid =*/ scan_string (scanner);
	break;
      case GDK_CONFIGURE:
	event->configure.x = scan_float (scanner);
	event->configure.y = scan_float (scanner);
	event->configure.width = scan_float (scanner);
	event->configure.height = scan_float (scanner);
	break;
      case GDK_MOTION_NOTIFY:
	event->motion.x = scan_float (scanner);
	event->motion.y = scan_float (scanner);
	/*event->motion.pressure =*/ scan_float (scanner);
	/*event->motion.xtilt =*/ scan_float (scanner);
	/*event->motion.ytilt =*/ scan_float (scanner);
	event->motion.state = scan_int (scanner);
	event->motion.is_hint = scan_int (scanner) != 0;
#warning "HERZI: add support for the device (maybe by name)"
	/*event->motion.source =*/ scan_symbol_int (scanner);
#warning "HERZI: free this?"
	/*event->motion.deviceid =*/ scan_string (scanner);
	break;
      case GDK_KEY_PRESS:
      case GDK_KEY_RELEASE:
	event->key.state = scan_int (scanner);
	event->key.keyval = scan_int (scanner);
	event->key.length = scan_int (scanner);
	event->key.string = g_strdup (scan_string (scanner));
	break;
      case GDK_ENTER_NOTIFY:
      case GDK_LEAVE_NOTIFY:
	subwindow_name = scan_string (scanner);
	event->crossing.subwindow = gerd_window_from_path (subwindow_name);
	if (!event->crossing.subwindow && subwindow_name && !GERD_IS_NULL_WINDOW_ID (subwindow_name))
	  {
	    g_warning ("unable to identify crossing subwindow: \"%s\"", subwindow_name);
	  }
	event->crossing.x = scan_float (scanner);
	event->crossing.y = scan_float (scanner);
	event->crossing.mode = scan_symbol_int (scanner);
	event->crossing.detail = scan_symbol_int (scanner);
	event->crossing.focus = scan_symbol_int (scanner) != FALSE;
	event->crossing.state = scan_int (scanner);
	break;
      case GDK_DELETE:
	break;
      default:
	g_scanner_error (scanner, "unknown event type (%d)", event->type);
	break;
      }
  
  if (!scanner->parse_errors && g_scanner_get_next_token (scanner) != ')')
    parse_error (scanner, ')');
  
  if (scanner->parse_errors)
    {
      free_event (event);
      
      return NULL;
    }
  else
    return event;
}

gboolean
gerd_complete_command (GScanner    *scanner,
		       GerdCommand *cmd)
{
  g_return_val_if_fail (scanner != NULL, FALSE);
  g_return_val_if_fail (cmd != NULL, FALSE);

  if (cmd->cmd == GERD_CMD_EVENT && !cmd->data.event->any.window)
    {
      GdkEvent *event = cmd->data.event;

      event->any.window = gerd_window_from_path (cmd->window_id);
      if (!event->any.window)
	{
	  cmd->retries += 1;
	  
	  return FALSE;
	}
      
      switch (cmd->data.event->type)
	{
	  gint xr, yr;
	case GDK_BUTTON_PRESS:
	case GDK_2BUTTON_PRESS:
	case GDK_3BUTTON_PRESS:
	case GDK_BUTTON_RELEASE:
	  xr = event->button.x;
	  yr = event->button.y;
	  gdk_window_translate (event->button.window, NULL, &xr, &yr);
	  event->button.x_root = xr;
	  event->button.y_root = yr;
	  break;
	case GDK_MOTION_NOTIFY:
	  xr = event->motion.x;
	  yr = event->motion.y;
	  gdk_window_translate (event->motion.window, NULL, &xr, &yr);
	  event->motion.x_root = xr;
	  event->motion.y_root = yr;
	  break;
	case GDK_ENTER_NOTIFY:
	case GDK_LEAVE_NOTIFY:
	  xr = event->crossing.x;
	  yr = event->crossing.y;
	  gdk_window_translate (event->crossing.window, NULL, &xr, &yr);
	  event->crossing.x_root = xr;
	  event->crossing.y_root = yr;
	  break;
	default:
	  break;
	}
    }
  else if (cmd->cmd == GERD_CMD_PSTATE && !cmd->data.pstate.window)
    {
      cmd->data.pstate.window = gerd_window_from_path (cmd->window_id);
      if (!cmd->data.pstate.window && !GERD_IS_NULL_WINDOW_ID (cmd->window_id))
	{
	  cmd->retries += 1;

	  return FALSE;
	}
    }

  return TRUE;
}

static GerdCommand*
gerd_scan_command (GScanner *scanner)
{
  GerdCommand *cmd;

  cmd = g_new0 (GerdCommand, 1);
  cmd->cmd = GERD_CMD_EOF;
  cmd->retries = 0;

  if (g_scanner_get_next_token (scanner) != '(')
    {
      if (scanner->token != G_TOKEN_EOF)
	parse_error (scanner, '(');
      else
	scanner->parse_errors = 1;
    }
  else if (g_scanner_get_next_token (scanner) != G_TOKEN_IDENTIFIER)
    parse_error (scanner, G_TOKEN_IDENTIFIER);
  else if (strcmp (scanner->value.v_identifier, "wait") == 0)
    {
      cmd->cmd = GERD_CMD_DELAY;
      cmd->data.delay = scan_int (scanner);
    }
  else if (strcmp (scanner->value.v_identifier, "event") == 0)
    {
      cmd->cmd = GERD_CMD_EVENT;
      cmd->data.event = parse_event (scanner, &cmd->window_id);
    }
  else if (strcmp (scanner->value.v_identifier, "warp") == 0)
    {
      cmd->cmd = GERD_CMD_WARP;
      cmd->data.warp.root_x = scan_int (scanner);
      cmd->data.warp.root_y = scan_int (scanner);
    }
  else if (strcmp (scanner->value.v_identifier, "pstate") == 0)
    {
      cmd->cmd = GERD_CMD_PSTATE;
      cmd->window_id = g_strdup (scan_string (scanner));
      cmd->data.pstate.window = NULL;
      cmd->data.pstate.win_x = scan_int (scanner);
      cmd->data.pstate.win_y = scan_int (scanner);
      cmd->data.pstate.mod_mask = scan_int (scanner);
    }
  else
    g_scanner_error (scanner, "unknown command \"%s\"", scanner->value.v_identifier);

  if (!scanner->parse_errors && g_scanner_get_next_token (scanner) != ')')
    parse_error (scanner, ')');

  if (scanner->parse_errors)
    {
      gerd_free_command (cmd);
      cmd = NULL;
    }

  return cmd;
}

GerdCommand*
gerd_peek_command (GScanner *scanner)
{
  g_return_val_if_fail (scanner != NULL, NULL);

  if (!scanner->user_data)
    scanner->user_data = gerd_scan_command (scanner);

  return scanner->user_data;
}

GerdCommand*
gerd_get_next_command (GScanner *scanner)
{
  GerdCommand *cmd;

  g_return_val_if_fail (scanner != NULL, NULL);

  cmd = gerd_peek_command (scanner);
  scanner->user_data = NULL;

  return cmd;
}

void
gerd_free_command (GerdCommand *command)
{
  g_return_if_fail (command != NULL);

  switch (command->cmd)
    {
    case GERD_CMD_EVENT:
      if (command->data.event)
	free_event (command->data.event);
      break;
    default:
      break;
    }
  g_free (command->window_id);
  g_free (command);
}
